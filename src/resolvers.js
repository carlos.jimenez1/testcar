"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
const _default = {
  Query: {
    allCars: async function allCars(parent, args, _ref) {
      var Car = _ref.Car;
      var cars = await Car.find();
      return cars.map(function (x) {
        x._id = x._id.toString();
        return x;
      });
    }
  },
  Mutation: {
    createCar: async function createCar(parent, args, _ref2) {
      var Car = _ref2.Car;
      var car = await new Car(args).save();
      car._id = car._id.toString();
      return car;
    }
  }
};
exports.default = _default;
