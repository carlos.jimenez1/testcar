"use strict";

const _express = _interopRequireDefault(require("express"));

const _mongoose = _interopRequireDefault(require("mongoose"));

const _Car = _interopRequireDefault(require("./models/Car"));

const _apolloServerExpress = require("apollo-server-express");

const _schema = _interopRequireDefault(require("./schema"));

const _resolvers = _interopRequireDefault(require("./resolvers"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const app = (0, _express.default)();

_mongoose.default.connect('mongodb://localhost/graphql-mongo', {
  useNewUrlParser: true
}).then(function () {
  return console.log('connected to db');
}).catch(function (err) {
  return console.log(err);
});

// settings
app.set('port', process.env.PORT || 3000);
var SERVER = new _apolloServerExpress.ApolloServer(_defineProperty({
  typeDefs: _schema.default,
  resolvers: _resolvers.default,
  context: {
    Car: _Car.default
  },
  introspection: true,
  playground: true
}, "playground", {
  endpoint: "http://localhost:3000/graphql",
  settings: {
    'editor.theme': 'dark'
  }
}));
SERVER.applyMiddleware({
  app: app
}); // start the server

app.listen(app.get('port'), function () {
  console.log('server on port', app.get('port'));
});
