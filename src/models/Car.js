"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

const _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const Schema = _mongoose.default.Schema;
const CarSChema = new Schema({
  name: String
});

const Car = _mongoose.default.model('cars', CarSChema);

const _default = Car;
exports.default = _default;
