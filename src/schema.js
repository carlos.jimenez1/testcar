"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = "\n  type Car {\n    _id: String!\n    name: String!\n  }\n\n  type Query {\n    allCars: [Car!]!\n  }\n\n  type Mutation {\n    createCar(name: String!): Car!\n  }\n\n";
exports.default = _default;
